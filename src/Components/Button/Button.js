import React from 'react';
import './Button.css';

const Button = ({ label, onClick, type }) => {

    let btnClassName;
    let actualLabel;

    if (label.length > 20) {
        actualLabel = label.split(0, 20);
    } else {
        actualLabel = label;
    }

    if (type === 'success') {
        btnClassName = 'btn btn-success'
    } else if (type === 'danger') {
        btnClassName = 'btn btn-danger'
    } else {
        btnClassName = 'btn'
    }

    return <button className={btnClassName} onClick={onClick}>{actualLabel}</button>
} 

export default Button;
