import React from 'react';
import Button from '../Button/Button'
import '../Modal/Modal.css';



const Modal = ({onChange, onSubmit, onClose, inputValue, buttonLabel}) => {

    return (
        <div className="modal">
            <div className="modal-content">
                <span className="close" onClick={onClose}>&times;</span>
                <div>
                    <input onChange={onChange} value={inputValue} type="text"/>
                    <Button onClick={onSubmit} label={buttonLabel} type={'success'} display={'none'} />
                </div>
            </div>
        </div>
    )
} 

export default Modal;