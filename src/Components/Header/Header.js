import React from 'react';
import './Header.css'

const Header = (props) => {


    return (
        <header>

          <div className='header-container'>
            <div className='container' >
              <h1>My Todo List</h1>
            </div>
            <form className='container'>
              <label htmlFor="newTodoInput"></label>
              <input placeholder="New element..." onChange={props.updateNewTodoName} value={props.inputValue} type="text"/>
              <button className='addBtn' onClick={props.addTodo}>ADD</button>
            </form>

          </div>
        </header>

    )
} 

export default Header;

