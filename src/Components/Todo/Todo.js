import React from 'react';
import Button from '../Button/Button'
import './Todo.css'

const Todo = ({ todo, onEdit, onDelete, onStatusChange }) => {
  const { title, id, extra } = todo;

    return (
      <div className='container'>
        <li>
          <Button onClick={() => { onDelete(id) }} label={'USUŃ'} type={'danger'}/>
          <Button
            onClick={() => {onStatusChange(id, extra) }}
            label={extra === '1' ? 'COFNIJ' : 'OK'}
            type={extra === '1' ? 'danger' : 'success'} />
          <Button onClick={() => {onEdit(title, id) }} label={'EDYTUJ'} />
          <span className={extra === "1" ? 'todo-done' : '' }>{title}</span>
      </li>
      </div>


    )
} 

export default Todo;
