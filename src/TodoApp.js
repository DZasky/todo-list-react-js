import React from 'react';
import axios from 'axios';
import Header from './Components/Header/Header';
import Modal from './Components/Modal/Modal'
import Todo from './Components/Todo/Todo'
import Loader from './Components/Loader/Loader'
import './TodoApp.css';

class TodoApp extends React.Component {
  constructor() {
    super();

    this.state = {
      newTodoName: '',
      editedTodoName: '',
      editedTodoId: '',
      todoList: [],
      isModalOpen: false, 
      isLoading: false,
    };
  }

  addNewTodo = (event) => {
  event.preventDefault();
  const { newTodoName } = this.state;
  axios.post('http://195.181.210.249:3000/todo/', { title: newTodoName, extra: "0" }).then(() => {
  this.getAndRenderTodos();
  this.setState({ newTodoName: '' })
  })
  }


  updateNewTodoName = event => {
    this.setState({ newTodoName: event.target.value });
  }

  updateEditedTodoName = event => {
    this.setState({ editedTodoName: event.target.value });
  }

  removeTodoAndRenderList = id => {
    axios.delete(`http://195.181.210.249:3000/todo/${id}`).then(() => {
      this.getAndRenderTodos();
    });
  }

  setAsDoneAndRenderList = (id, status) => {
    let extra = status;

    if (extra === "0") {
      extra = "1";
    } else {
      extra = "0";
    }

    axios.put(`http://195.181.210.249:3000/todo/${id}`, { extra }).then(() => {
      this.getAndRenderTodos();
    });
  }

  openEditModal = (title, id) => {
    this.setState({
      editedTodoName: title,
      editedTodoId: id,
      isModalOpen: true,
    });
  }

  closeEditModal = () => {
    this.setState({
      isModalOpen: false,
    });
  }

  updateTodoAndRenderList = () => {
    this.setState({ isLoading: true})



    const { editedTodoId, editedTodoName } = this.state;
    axios.put(`http://195.181.210.249:3000/todo/${editedTodoId}`, { title: editedTodoName }).then(() => {
      this.setState({
        editedTodoName: '',
        editedTodoId: '',
        isModalOpen: false,
      })
      this.getAndRenderTodos();
    })
  }

  getAndRenderTodos = () => {
    axios.get('http://195.181.210.249:3000/todo/').then(resp => {
      const todosFromServer = resp.data;
      this.setState({
        todoList: todosFromServer,
        isLoading: false,
      })
    });
  }

  componentDidMount() {
    this.getAndRenderTodos();
  }

  render() {
    return (
      <main>
        {this.state.isLoading && <div className="loader"><Loader /></div>}
        <Header 
          updateNewTodoName={this.updateNewTodoName}
          addTodo={this.addNewTodo}  
          inputValue={this.state.newTodoName} 
        />

        {this.state.isModalOpen && (
          <Modal 
          onChange={this.updateEditedTodoName} 
          onSubmit={this.updateTodoAndRenderList}
          onClose={this.closeEditModal}
          inputValue={this.state.editedTodoName}
          buttonLabel={'ZMIEŃ'}
          />
        )}

        <section>
          <ul>
            {this.state.todoList.map(
              todo =>
                <Todo
                  key={todo.id}
                  todo={todo}
                  onEdit={this.openEditModal}
                  onDelete={this.removeTodoAndRenderList}
                  onStatusChange={this.setAsDoneAndRenderList}
                />
            )}
          </ul>
        </section> 
      </main>
    );
  }
};

export default TodoApp;
