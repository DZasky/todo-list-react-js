# Todo List - React JS

My final project of Frontend Bootcamp, module 2: Javascript (Vanilla JS) has been remastered into React JS (module 3). This project helped me to improve my skills in:

- components
- communication with backend
- debugging